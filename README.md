- dropbox
[sharing files](https://www.dropbox.com/sh/92145k0g53lge5w/AACyl-oGFf0GVF1CC_7PDS4Na?dl=0)
- instagantt
[project task](https://instagantt.com/r#projects/5b3980b25250ee0326465fa7/728897921444217)
- wireframe
[in excel](https://docs.google.com/spreadsheets/d/1Mk88cq5IL2PHS1NFAgsmGNQh3FV3V_0ONkXL4Wp4CTE/edit?usp=drive_web&ouid=106431561082407740939)
- design concept forum
[repo](https://gitlab.com/maverick-magang/nls-forum-design-concept)
- api documentation
[WIP](https://whiteboard-xjmvilpwqb.now.sh/)
[repo](https://gitlab.com/maverick-magang/nls-forum-api)

### List Functionality

**Status** : WIP (work in progress), v, -

|   NO | Parent                                                 | Status | Description |
|------|--------------------------------------------------------|--------|-------------|
|    1 | login as parent with nis                               | WIP    | -           |
|    2 | create parent dashboard                                | v      | -           |
|    3 | go to forum guru                                       | v      | -           |
|    4 | go to forum teacher                                    | v      | -           |
|    5 | go to jadwal                                           | v      | -           |
|  *6* | GET nis and student name                               | -      | -           |
|    7 | create new question form                               | v      |             |
|  *8* | GET matpel list to dropdown form                       |        |             |
|  *9* | submit form and save data to DB                        |        |             |
|   10 | add popup success after confirm form                   |        |             |
| *11* | sent notification to guru after parent create question |        |             |

|  NO | Teacher                                                   | Status | Status |
|-----|-----------------------------------------------------------|--------|--------|
|   1 | login as teacher with nip                                 | WIP    | -      |
| *2* | teacher dapat melihat post yang pernah dikomentari        | -      | -      |
| *3* | teacher dapat mengisi & mengajukan jadwal mingguan        | -      | -      |
|   4 | teacher dapat mengisi & mengajukan jadwal harian          | -      | -      |
|   5 | teacher dapat melihat semua jadwal yang sudah tervalidasi | -      | -      |

| NO | Admin                                | Status | Status |
|----|--------------------------------------|--------|--------|
|  1 | login as admin with nip              | WIP    | -      |
|  2 | admin bisa melihat semua post        | -      | -      |
|  3 | admin memvalidasi/acc rencana jadwal | -      | -      |
|    |                                      | -      | -      |


- 
